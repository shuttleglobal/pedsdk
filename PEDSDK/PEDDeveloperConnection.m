//
//  PEDDeveloperConnection.m
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "PEDDeveloperConnection.h"
#import "PEDConnection+Private.h"
#import "PEDPaymentRequest.h"
#import "PEDPaymentRequest+Private.h"
#import "PEDDeveloperPaymentViewController.h"
#import "PEDUtil.h"

@implementation PEDDeveloperConnection

- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock {
    self = [super initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock];
    
    self.status = kPEDStatusConnecting;
    
    id _self = self;
    dispatch_async( dispatch_get_main_queue(), ^{
        if ([self.configuration valueForKeyPath:@"pedValues.errorSDK"]) {
            self.status = kPEDStatusErrorSDK;
            self.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:kPEDConnectionErrorInvalidPedImplemention userInfo:@{NSLocalizedDescriptionKey: @"Could not start SDK, probably an invalid API Key."}];
        } else {
            self.status = kPEDStatusConnectedReady;
        }
        statusUpdateBlock(_self);
    });
    
    return self;
}

- (NSString *) pedType {
    return kPEDTypeDeveloper;
}

- (NSString *) pedName {
    return @"Virtual PED";
}

- (NSString *) pedDescription {
    return [NSString stringWithFormat:@"%@ %@", self.pedName, @"0.1"];
}

- (NSDictionary *) currencies {
    return @{
             @"GBP": @{
                     @"methods": @[kPEDCardTypeMastercard, kPEDCardTypeVisa, kPEDCardTypeAMEX,
                                   kPEDCardTypeMaestro, kPEDCardTypeVPay, kPEDCardTypeVisaElectron,
                                   kPEDCardTypeJCB, kPEDCardTypeDiners],
                     @"minCharge": [NSDecimalNumber decimalNumberWithString:@"1"],
                     @"maxCharge": [NSDecimalNumber decimalNumberWithString:@"10000"]
                     },
             @"USD": @{
                     @"methods": @[kPEDCardTypeMastercard, kPEDCardTypeVisa, kPEDCardTypeAMEX,
                                   kPEDCardTypeMaestro, kPEDCardTypeVPay, kPEDCardTypeVisaElectron,
                                   kPEDCardTypeJCB, kPEDCardTypeDiners],
                     @"minCharge": [NSDecimalNumber decimalNumberWithString:@"1"],
                     @"maxCharge": [NSDecimalNumber decimalNumberWithString:@"10000"]
                     }
             };
}

- (BOOL) canRefundFull {
    return true;
}
- (BOOL) canRefundPartial {
    return true;
}

- (void) cancelPayment:(PEDPaymentRequest *)payment {
    payment.status = kPEDPaymentStatusCancelled;
}

- (PEDPaymentRequest *) startPaymentInCurrency:(NSString*)currency forAmount:(NSDecimalNumber *)amount withReference:(NSString *)reference options:(NSDictionary *)options statusUpdateBlock:(void(^)(PEDPaymentRequest * request))statusUpdateBlock {
    PEDPaymentRequest * request = [[PEDPaymentRequest alloc] initWithConnection:self forAmount:amount inCurrency:currency withReference:reference statusUpdateBlock:statusUpdateBlock];
    
//    PEDDeveloperConnection * _self = self;
    PEDDeveloperPaymentViewController * vc = [[PEDDeveloperPaymentViewController alloc] initWithRequest:request andCallback:statusUpdateBlock];
    [PEDUtil.topMostController presentViewController:vc animated:true completion:nil];
    
    return request;
}

@end
