//
//  PEDPaymentRequest.m
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "PEDPaymentRequest.h"
#import "PEDPaymentRequest+Private.h"

@interface PEDPaymentRequest()
@property (nonatomic, retain) NSMutableDictionary * privateValues;
@end

@implementation PEDPaymentRequest

- (void) setStatus:(NSString *)status {
    _status = status;

    if (self.statusUpdateBlock) {
        PEDPaymentRequest * _self = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            _self.statusUpdateBlock(_self);
        });
    }
}

- (void) updateNonce {
    NSInteger txnCounter = [[NSUserDefaults standardUserDefaults] integerForKey:@"PEDSDKPaymentRequestCounter"];
    [[NSUserDefaults standardUserDefaults] setInteger:txnCounter + 1 forKey:@"PEDSDKPaymentRequestCounter"];
    _nonce = [NSString stringWithFormat:@"%li", txnCounter];
}

- (id) initWithConnection:(PEDConnection *) connection forAmount:(NSDecimalNumber *) amount inCurrency:(NSString *) currency withReference:(NSString *)reference  statusUpdateBlock:(void(^)(PEDPaymentRequest * request))statusUpdateBlock{
    self = [super init];


    _connection = connection;
    _amount = amount;
    _currency = currency;
    _reference = reference;
    [self updateNonce];

    self.statusUpdateBlock = statusUpdateBlock;
    self.privateValues = [NSMutableDictionary dictionary];
    return self;
}


@end
