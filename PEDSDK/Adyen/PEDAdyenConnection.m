//
//  PEDiZettleConnection.m
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "PEDAdyenConnection.h"
#import "PEDPaymentRequest.h"
#import "PEDRefundRequest.h"
#import "PEDConnection+Private.h"
#import "PEDPaymentRequest+Private.h"
#import "PEDRefundRequest+Private.h"
//#import "PEDPaymentViewController.h"
#import "PEDUtil.h"

// Configure your terminal as such: https://docs.adyen.com/developers/point-of-sale/user-manuals

@interface PEDAdyenConnection()

@property (nonatomic, assign) BOOL debug;

@property (nonatomic, assign) BOOL sandbox;
@property (nonatomic, retain) NSString * merchantAccount;
@property (nonatomic, retain) NSString * apiKey;
@property (nonatomic, retain) NSArray * connectedTerminals;

@property (nonatomic, retain) NSURLSessionDataTask * connectedTerminalsDataTask;
@property (nonatomic, retain) NSURLSessionDataTask * paymentDataTask;
@property (nonatomic, retain) NSURLSessionDataTask * refundDataTask;
@property (nonatomic, retain) NSURLSessionDataTask * cancelDataTask;

@end

@implementation PEDAdyenConnection


- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    self = [super initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock];
    
    [self downloadConfig];
    
    self.debug = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"PEDSDKDEBUG"];

    return self;
}

- (void) downloadConfig {
    if (!self.status || [self.status isEqualToString:kPEDStatusConnectingFailed]) {
        self.status = kPEDStatusConnecting;
        PEDAdyenConnection * _self = self;
        

        //        dispatch_async( dispatch_get_main_queue(), ^{
//            _self.sandbox = TRUE;
//            _self.merchantAccount = @"PayWithBoltCOM";
//            _self.apiKey = @"AQEmhmfuXNWTK0Qc+iSAk30PrvCTaoRBGYPBxtOKJaYWVPCZr/qZ0k4QwV1bDb7kfNy1WIxIIkxgBw==-GQ5SKCgksasZrUZVPOHO3vPmxgKeeNOoyX1cqU7cCxg=-94AqBk6G6DD46VEa";
//            _self.status = kPEDStatusConnectedReady;
//            _self.statusUpdateBlock(_self);
//            [_self refreshDeviceList];
//        });
//
//        return;
        
        NSURL * url = [NSURL URLWithString:[self.configuration valueForKeyPath:@"pedValues.configuration"]];

        if (self.debug) {
            NSLog(@"PEDAdyenConnection.downloadConfiguration: %@", url.absoluteString);
        }
        
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (error) {
                // Dont call callback because we can't handle an error without a PEDConnection
                // On network status change we should try to connect again
                _self.status = kPEDStatusConnectingFailed;
                _self.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Could not connect to server to retrieve credentials"]}];
            } else {
                NSDictionary * config = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                _self.configuration = config;
                
                dispatch_async( dispatch_get_main_queue(), ^{
                    _self.sandbox = [config[@"sandbox"] isEqual:@"TRUE"];
                    _self.apiKey = config[@"apiKey"];
                    _self.merchantAccount = config[@"merchantAccount"];
                    _self.status = kPEDStatusConnectedReady;
                    _self.statusUpdateBlock(_self);
                    [_self refreshDeviceListOnCompletion:^(PEDWhiteLabelledConnection * _Nonnull connection) {
                        // Select first device?
                    }];
                });
                
            }
        }] resume];
    }
}


- (NSURLSessionDataTask *) adyenDataTask:(NSString *)url withRequest:(NSDictionary *)request completionHander: (void (^)(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error)) completionHander {
    
    if (self.debug) {
        NSLog(@"Request [%@]: %@", url, [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:request options:0 error:nil] encoding:NSUTF8StringEncoding]);
    }
    
    NSMutableURLRequest * urlRequest = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:url]];
    urlRequest.HTTPMethod = @"POST";
    [urlRequest addValue:self.apiKey forHTTPHeaderField:@"x-api-key"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    urlRequest.HTTPBody =  [NSJSONSerialization dataWithJSONObject:request options:0 error:nil];
    
    NSURLSessionDataTask * dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"PEDAdyenConnection Result [%@] ERROR: %@", url, error.localizedDescription);
        } else if (self.debug) {
            NSLog(@"Result [%@]: %@", url, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
        }
        
        completionHander(data, response, error);
    }];
    
    return dataTask;
}



-(void) refreshDeviceListOnCompletion:(void (^)(PEDWhiteLabelledConnection *)) completionBlock {
    NSDictionary * request = @{
        @"merchantAccount": self.merchantAccount
        };

    PEDAdyenConnection *_self = self;
    self.connectedTerminalsDataTask = [self adyenDataTask:[NSString stringWithFormat:@"%@/connectedTerminals", self.endPoint] withRequest:request completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (error) {
            // Dont call callback because we can't handle an error without a PEDConnection
            // On network status change we should try to connect again
            _self.status = kPEDStatusConnectingFailed;
            _self.error = error;//[NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: error.}];
        } else {
            NSDictionary * result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            _self.connectedTerminals = result[@"uniqueTerminalIds"];
            _self.connectedTerminalsDataTask = nil;
            
            dispatch_async( dispatch_get_main_queue(), ^{
                _self.statusUpdateBlock(_self);
            });
        }
        completionBlock(_self);
    }];
    
    [self.connectedTerminalsDataTask resume];
}



- (void) reachabilityChanged:(NSObject *) notification {
    if ([self.status isEqualToString:kPEDStatusConnectingFailed]) {
        [self downloadConfig];
    }
    
    [super reachabilityChanged:notification];
}

- (NSString *) pedType {
    return kPEDTypeAdyen;
}

- (NSString *) pedName {
    return @"Adyen";
}
- (NSString *) pedDescription {
    return self.selectedDevice ? [NSString stringWithFormat:@"%@", self.selectedDevice[@"name"]] : nil;
}

- (NSDictionary *) currencies {
    return @{};
}

- (BOOL) canRefundFull {
    return true;
}
- (BOOL) canRefundPartial {
    return false;
}

- (NSString *) endPoint {
    return self.sandbox ? @"https://terminal-api-test.adyen.com" : @"https://terminal-api-live.adyen.com";
}

- (NSString *) applicationInfo {
    NSDictionary * applicationInfo =
        @{
          @"applicationInfo": @{
                @"merchantApplication":@{
                      @"name": [[NSBundle mainBundle] objectForInfoDictionaryKey:(id)kCFBundleExecutableKey],
                      @"version": [[NSBundle mainBundle] objectForInfoDictionaryKey:(id)kCFBundleVersionKey]
                      },
                @"merchantDevice": @{
                    @"os": [[UIDevice currentDevice] name],
                    @"version":  [NSString stringWithFormat:@"%@ %@", [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion]],
                    @"reference": [[[UIDevice currentDevice] identifierForVendor] UUIDString]
                },
                @"externalPlatform": @{
                    @"integrator": @"Pay with Bolt",
                    @"name": @"PEDSDK",
                    @"version": @"0.3.1"
                }
            }
          };
    
    return [[NSJSONSerialization dataWithJSONObject:applicationInfo options:0 error:nil] base64EncodedStringWithOptions:0];
}

- (NSURLSessionDataTask *) dataTaskForPaymentRequest:(PEDPaymentRequest*)paymentRequest {
    NSDateFormatter *jsonFormat = [[NSDateFormatter alloc] init];
    [jsonFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [jsonFormat setTimeZone:gmt];
    
    NSDictionary * poiRequest =
    @{
      @"SaleToPOIRequest": @{
              @"MessageHeader": @{
                      @"ServiceID": paymentRequest.nonce,
                      @"MessageType": @"Request",
                      @"MessageClass": @"Service",
                      @"ProtocolVersion": @"3.0",
                      @"POIID": self.selectedDevice[@"id"],
                      @"SaleID": [[[UIDevice currentDevice] identifierForVendor] UUIDString],
                      @"MessageCategory": @"Payment"
                      },
              @"PaymentRequest": @{
                      @"SaleData": @{
                              @"SaleTransactionID": @{
                                      @"TransactionID": paymentRequest.reference,
                                      @"TimeStamp": [jsonFormat stringFromDate:[NSDate date]]
                                      },
                              @"SaleToAcquirerData": self.applicationInfo,
                              },
                      @"PaymentTransaction": @{
                              @"AmountsReq": @{
                                      @"Currency": paymentRequest.currency,
                                      @"RequestedAmount": paymentRequest.amount
                                      }
                              }
                      },
              }
      };
    
    PEDAdyenConnection * _self = self;
    return [self adyenDataTask:[NSString stringWithFormat:@"%@/sync", self.endPoint] withRequest:poiRequest completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable zresponse, NSError * _Nullable error) {
        
        _self.paymentDataTask = nil;
        
        if (error) {
            [self recoverPaymentRequest:paymentRequest];
        } else {
            NSDictionary * response = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

            if (error) {
                [self recoverPaymentRequest:paymentRequest];
            } else {
                [self handlePaymentRequest:paymentRequest response:response];
            }
        }
        
    }];
}


-(void) recoverPaymentRequest:(PEDPaymentRequest*)paymentRequest {
    NSDateFormatter *jsonFormat = [[NSDateFormatter alloc] init];
    [jsonFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [jsonFormat setTimeZone:gmt];

    paymentRequest.recoveryAttempt++;
    
    NSDictionary * poiRequest =
    @{
      @"SaleToPOIRequest": @{
              @"MessageHeader": @{
                      @"ServiceID": [NSString stringWithFormat:@"%@R%ld", paymentRequest.nonce, (long)paymentRequest.recoveryAttempt],
                      @"MessageType": @"Request",
                      @"MessageClass": @"Service",
                      @"ProtocolVersion": @"3.0",
                      @"POIID": self.selectedDevice[@"id"],
                      @"SaleID": [[[UIDevice currentDevice] identifierForVendor] UUIDString],
                      @"MessageCategory": @"TransactionStatus"
                      },
              @"TransactionStatusRequest": @{
                      @"MessageReference": @{
                              @"MessageCategory": @"Payment",
                              @"SaleID": [[[UIDevice currentDevice] identifierForVendor] UUIDString],
                              @"ServiceID": paymentRequest.nonce
                              }
                      }
              }
      };
      
      PEDAdyenConnection * _self = self;
      self.paymentDataTask = [self adyenDataTask:[NSString stringWithFormat:@"%@/sync", self.endPoint] withRequest:poiRequest completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable zresponse, NSError * _Nullable error) {
          
          _self.paymentDataTask = nil;
          
          if (error) {
              // Catch a circular loop
              [self recoverPaymentRequest:paymentRequest];
          } else {
              NSDictionary * response = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];

              if (error) {
                  // Catch a circular loop
                  [self recoverPaymentRequest:paymentRequest];
              } else {
                  [self handlePaymentRequest:paymentRequest response:response];
              }
              
          }
      }];
    
    [self.paymentDataTask resume];
  }


- (void) startPaymentRequest:(PEDPaymentRequest*)paymentRequest {
    paymentRequest.cancelRequested = false;
    paymentRequest.error = nil;
    paymentRequest.status = kPEDPaymentStatusInProgress;

    self.paymentDataTask = [self dataTaskForPaymentRequest:paymentRequest];
    [self.paymentDataTask resume];
}

-(void) handlePaymentRequest:(PEDPaymentRequest*)paymentRequest response:(NSDictionary *) response {
    NSDictionary * paymentResponse = [response valueForKeyPath:@"SaleToPOIResponse.PaymentResponse"] ?  [response valueForKeyPath:@"SaleToPOIResponse.PaymentResponse"] : [[response valueForKeyPath:@"SaleToPOIResponse.TransactionStatusResponse.Response.Result"] isEqualToString:@"Success"] ? [response valueForKeyPath:@"SaleToPOIResponse.TransactionStatusResponse.RepeatedMessageResponse.RepeatedResponseMessageBody.PaymentResponse"] : [response valueForKeyPath:@"SaleToPOIResponse.TransactionStatusResponse"];
    
    if ([[response valueForKeyPath:@"SaleToPOIRequest.EventNotification.EventToNotify"] isEqualToString:@"Reject"]) {
        paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[response valueForKeyPath:@"SaleToPOIRequest.EventNotification.EventDetails"]}];
        paymentRequest.status = kPEDPaymentStatusCancelled;
    } else if ([[paymentResponse valueForKeyPath:@"Response.Result"] isEqualToString:@"Failure"]) {
        if ([[paymentResponse valueForKeyPath:@"Response.ErrorCondition"] isEqualToString:@"Cancel"]) {
            if (![paymentRequest.status isEqualToString:kPEDPaymentStatusCancelled]) {
                paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Customer cancelled transaction."]}];
                paymentRequest.status = kPEDPaymentStatusCancelled;
            }
        } else if ([[paymentResponse valueForKeyPath:@"Response.ErrorCondition"] isEqualToString:@"Aborted"]) {
            if (![paymentRequest.status isEqualToString:kPEDPaymentStatusCancelled]) {
                paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat: @"Merchant cancelled transaction."]}];
                paymentRequest.status = kPEDPaymentStatusCancelled;
            }
        } else if ([[paymentResponse valueForKeyPath:@"Response.ErrorCondition"] isEqualToString:@"NotFound"]) {
            if (![paymentRequest.status isEqualToString:kPEDPaymentStatusCancelled]) {
                paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat: @"Transaction could not be started."]}];
                paymentRequest.status = kPEDPaymentStatusError;
            }
        } else if ([[paymentResponse valueForKeyPath:@"Response.ErrorCondition"] isEqualToString:@"Busy"] || [[paymentResponse valueForKeyPath:@"Response.ErrorCondition"] isEqualToString:@"NotAllowed"]) {
            paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: @"Card reader busy"}];
            paymentRequest.status = kPEDPaymentStatusError;
        } else if ([[paymentResponse valueForKeyPath:@"Response.ErrorCondition"] isEqualToString:@"InProgress"]) {
            // If the transaction is in progress, keep checking every second
            NSLog(@"Transaction in progress, retry in a second");
            PEDAdyenConnection * _self = self;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [_self recoverPaymentRequest:paymentRequest];
            });
        } else {
            paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [paymentResponse valueForKeyPath:@"Response.ErrorCondition"] ? [paymentResponse valueForKeyPath:@"Response.ErrorCondition"] : @"no description"}];
            paymentRequest.status = kPEDPaymentStatusDeclined;
        }
        // CATCH BUSY
        
    } else if ([[paymentResponse valueForKeyPath:@"Response.Result"] isEqualToString:@"Success"]) {
        
        NSDictionary * gatewayReference = @{
                                           @"PSPRef": [paymentResponse valueForKeyPath:@"PaymentResult.PaymentAcquirerData.AcquirerTransactionID.TransactionID"] ? [paymentResponse valueForKeyPath:@"PaymentResult.PaymentAcquirerData.AcquirerTransactionID.TransactionID"] : [NSNull null],
                                           @"POITransactionID": [paymentResponse valueForKeyPath:@"POIData.POITransactionID"] ? [paymentResponse valueForKeyPath:@"POIData.POITransactionID"] : [NSNull null],
                                           @"PaymentAcquirerData": [paymentResponse valueForKeyPath:@"PaymentResult.PaymentAcquirerData"] ? [paymentResponse valueForKeyPath:@"PaymentResult.PaymentAcquirerData"] : [NSNull null],
                                           @"SaleData": [paymentResponse valueForKeyPath:@"SaleData"] ? [paymentResponse valueForKeyPath:@"SaleData"] : [NSNull null],

                                           };
        
        paymentRequest.methodType = [paymentResponse valueForKeyPath:@"PaymentResult.PaymentInstrumentData.CardData.PaymentBrand"];
        paymentRequest.methodDescription = [paymentResponse valueForKeyPath:@"PaymentResult.PaymentInstrumentData.CardData.MaskedPan"];
        paymentRequest.authorisationCode = [paymentResponse valueForKeyPath:@"PaymentResult.PaymentAcquirerData.ApprovalCode"];
        paymentRequest.gatewayReference = [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:gatewayReference options:0 error:nil] encoding:NSUTF8StringEncoding];

        paymentRequest.paymentValues = @{
                                         @"transactionId": paymentRequest.gatewayReference,
                                         @"poiTransactionId": [paymentResponse valueForKeyPath:@"POIData.POITransactionID.TransactionID"] ? [paymentResponse valueForKeyPath:@"POIData.POITransactionID.TransactionID"] : [NSNull null],
                                         @"paymentMethodType": paymentRequest.methodType ? paymentRequest.methodType : [NSNull null],
                                         @"paymentMethodDetail": paymentRequest.methodDescription ? paymentRequest.methodDescription : [NSNull null],
                                         
                                         @"authCode": paymentRequest.authorisationCode ? paymentRequest.authorisationCode : [NSNull null],
                                         @"adyenData": paymentResponse ? paymentResponse : [NSNull null]
                                         };
        paymentRequest.error = nil;
        paymentRequest.status = kPEDPaymentStatusSuccess;
    } else {
        paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Implementation error, unhandled response."}];
        paymentRequest.status = kPEDPaymentStatusCancelled;
        NSLog(@"UNHANDLED RESPONSE - PLEASE INFORM DEVELOPER");
    }
}

-(void) handleRefundRequest:(PEDRefundRequest*)refundRequest response:(NSDictionary *) response {
    NSDictionary * refundResponse = [response valueForKeyPath:@"SaleToPOIResponse.ReversalResponse"];
    
    if ([[response valueForKeyPath:@"SaleToPOIRequest.EventNotification.EventToNotify"] isEqualToString:@"Reject"]) {
        refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[response valueForKeyPath:@"SaleToPOIRequest.EventNotification.EventDetails"]}];
        refundRequest.status = kPEDPaymentStatusCancelled;
    } else if ([[refundResponse valueForKeyPath:@"Response.Result"] isEqualToString:@"Failure"]) {
        refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"%@ (%@)",[refundResponse valueForKeyPath:@"Response.ErrorCondition"], [refundResponse valueForKeyPath:@"Response.AdditionalResponse"]]}];
        refundRequest.status = kPEDPaymentStatusCancelled;
    } else if ([[refundResponse valueForKeyPath:@"Response.Result"] isEqualToString:@"Success"]) {
        refundRequest.authorisationCode = [refundResponse valueForKeyPath:@"POIData.POITransactionID"];
        refundRequest.gatewayReference = [refundResponse valueForKeyPath:@"POIData.POITransactionID.TransactionID"];
        refundRequest.paymentValues = @{
                                         @"transactionId": [refundResponse valueForKeyPath:@"POIData.POITransactionID.TransactionID"] ? [refundResponse valueForKeyPath:@"POIData.POITransactionID.TransactionID"] : [NSNull null],
                                         @"authCode": refundRequest.authorisationCode ? refundRequest.authorisationCode : [NSNull null],
                                         @"adyenData": refundResponse ? refundResponse : [NSNull null]
                                         };
        refundRequest.error = nil;
        refundRequest.status = kPEDPaymentStatusSuccess;
        
    } else {
        refundRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey:@"Implementation error, unhandled response."}];
        refundRequest.status = kPEDPaymentStatusCancelled;
        NSLog(@"UNHANDLED RESPONSE - PLEASE INFORM DEVELOPER");
    }
    
    
    
}

- (NSURLSessionDataTask *) dataTaskForRefundRequest:(PEDRefundRequest*)refundRequest {
    NSDateFormatter *jsonFormat = [[NSDateFormatter alloc] init];
    [jsonFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSTimeZone *gmt = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    [jsonFormat setTimeZone:gmt];

    NSError *error;
        
    // If JSON full formed
    NSDictionary * gatewayReference = [NSJSONSerialization JSONObjectWithData:[refundRequest.reference dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
    
    NSDictionary * poiRequest =
    @{
      @"SaleToPOIRequest": @{
              @"MessageHeader": @{
                      @"ServiceID": refundRequest.nonce,
                      @"MessageType": @"Request",
                      @"MessageClass": @"Service",
                      @"ProtocolVersion": @"3.0",
                      @"POIID": self.selectedDevice[@"id"],
                      @"SaleID": [[[UIDevice currentDevice] identifierForVendor] UUIDString],
                      @"MessageCategory": @"Reversal"
                      },
              @"ReversalRequest": @{
                  @"ReversedAmount": refundRequest.amount,
                  @"ReversalReason": @"MerchantCancel",
                  @"OriginalPOITransaction": @{
                      @"POITransactionID": [gatewayReference valueForKeyPath:@"POITransactionID"]
                  },
                  @"SaleData": @{
                      @"SaleToAcquirerData": [gatewayReference valueForKeyPath:@"SaleData.SaleToAcquirerData"] ? [gatewayReference valueForKeyPath:@"SaleData.SaleToAcquirerData"] : [NSString stringWithFormat:@"currency=%@", refundRequest.currency],
                      @"SaleTransactionID": [gatewayReference valueForKeyPath:@"SaleData.SaleTransactionID"] ? @{
                          @"TransactionID": [gatewayReference valueForKeyPath:@"SaleData.SaleTransactionID.TransactionID"],
                          @"TimeStamp": [jsonFormat stringFromDate:[NSDate date]]
                      } : [NSNull null]
                          
                  }
              }
            }
      };

    NSLog(@"After poiRequest");

    PEDAdyenConnection * _self = self;
    return [self adyenDataTask:[NSString stringWithFormat:@"%@/sync", self.endPoint] withRequest:poiRequest completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable zresponse, NSError * _Nullable error) {
        
        _self.paymentDataTask = nil;
        
        if (error) {
            refundRequest.error = error;
            refundRequest.status = kPEDPaymentStatusError;
        } else {
            NSDictionary * response = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
            [self handleRefundRequest:refundRequest response:response];
        }
        
    }];
}


- (PEDRefundRequest *) refundPaymentReference:(NSString*)reference amount:(NSDecimalNumber *)amount currency:(NSString *)currency withReason:(NSString *)reason statusUpdateBlock:(void(^)(PEDRefundRequest * request))statusUpdateBlock {
    
    PEDRefundRequest * request = [[PEDRefundRequest alloc] initWithConnection:self forTransactionReference:reference forAmount:amount inCurrency:currency withReason:reason statusUpdateBlock:statusUpdateBlock];
    
    if (!reference.length) {
        NSLog(@"Refund failed!: Reference Required.");
        request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Reference required."]}];
        request.status = kPEDPaymentStatusError;
    } else if (amount && ([amount isEqual:[NSDecimalNumber notANumber]] || (!amount.doubleValue))) {
        NSLog(@"Refund failed!: Invalid amount.");
        request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Invalid amount."]}];
        request.status = kPEDPaymentStatusError;
    } else if (!self.selectedDevice) {
        NSLog(@"Refund failed!: No Selected Device.");
        request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"No device selected."]}];
        request.status = kPEDPaymentStatusError;
    } else {
        self.refundDataTask = [self dataTaskForRefundRequest:request];
        [self.refundDataTask resume];
    }
    
    return request;
}



- (void) cancelPayment:(PEDPaymentRequest *)paymentRequest {
    paymentRequest.cancelRequested = TRUE;

    NSDictionary * request =
    @{
      @"SaleToPOIRequest": @{
              @"MessageHeader": @{
                      @"ServiceID": [NSString stringWithFormat:@"%@X", paymentRequest.nonce],
                      @"MessageType": @"Request",
                      @"MessageClass": @"Service",
                      @"ProtocolVersion": @"3.0",
                      @"POIID": self.selectedDevice[@"id"],
                      @"SaleID": [[[UIDevice currentDevice] identifierForVendor] UUIDString],
                      @"MessageCategory": @"Abort"
                      },
              @"AbortRequest" : @{
                      @"AbortReason" : @"MerchantAbort",
                      @"MessageReference" : @{
                              @"SaleID" : [[[UIDevice currentDevice] identifierForVendor] UUIDString],
                              @"ServiceID" : paymentRequest.nonce,
                              @"MessageCategory" : @"Payment"
                              }
                      }
              }
      };
    
    PEDAdyenConnection * _self = self;
    self.cancelDataTask = [self adyenDataTask:[NSString stringWithFormat:@"%@/sync", self.endPoint] withRequest:request completionHander:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        _self.cancelDataTask = nil;
    }];
    
    [self.cancelDataTask resume];
}


- (BOOL)popoverPresentationControllerShouldDismissPopover:(UIPopoverPresentationController *)popoverPresentationController {
    return false;
}

- (NSArray *) deviceList {
    NSMutableArray *devices = [NSMutableArray array];
    for (NSString * device in self.connectedTerminals) {
        [devices addObject:@{
                             @"id": device,
                             @"name": device
                             }];
    }
    return devices;
}



@end
