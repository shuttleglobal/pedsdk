//
//  PEDiZettleConnection.h
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//
// See:
// TBC

#import "PEDConfigDelegate.h"
#import "PEDWhiteLabelledConnection.h"

@interface PEDAdyenConnection : PEDWhiteLabelledConnection

- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate;

@end
