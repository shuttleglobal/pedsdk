//
//  PEDPaymentViewController.h
//  PEDSDK
//
//  Created by Phil Peters on 06/03/2019.
//  Copyright © 2019 Pay With Bolt. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class PEDPaymentRequest, PEDConnection;

@interface PEDPaymentViewController : UITableViewController

- (id) initWithPaymentRequest:(PEDPaymentRequest *) request forConnection:(PEDConnection *) connection;

- (void) paymentRequestStatusUpdate:(PEDPaymentRequest *) paymentRequest;

- (void) cancelButtonClick;
@end

NS_ASSUME_NONNULL_END
