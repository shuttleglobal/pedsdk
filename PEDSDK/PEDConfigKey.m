//
//  PEDConfigKey.m
//  bpos
//
//  Created by Paul Keith on 02/07/2018.
//  Copyright © 2018 Brightpearl. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PEDConfigKey.h"

NSString *const kiZettleClientId = @"iZettleClientID";
NSString *const kiZettleCallbackURL = @"iZettleCallbackURL";
