//
//  PEDConfigDelegate.h
//  BrightpearlPOS
//
//  Created by Paul Keith on 02/07/2018.
//  Copyright © 2018 Brightpearl. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef PEDConfigDelegate_h
#define PEDConfigDelegate_h

// A delegate that can be queried for configuration values.
@protocol PEDConfigDelegate

// Get the configuration value for the specified key, or nil
// if no value exists for that key.  The key is expected to
// be defined in PEDConfigKey.h.
- (NSString *)valueFor:(NSString *)key;

@end

#endif /* PEDConfigDelegate_h */
