//
//  PEDConnection.m
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "PEDConnection.h"
#import "PEDConnection+Private.h"
#import "PEDiZettleConnection.h"
#import "PEDAdyenConnection.h"
#import "PEDUSAEpayConnection.h"
#import "PEDMonerisConnection.h"
#import "PEDSquareConnection.h"
#import "PEDDeveloperConnection.h"
#import "PEDReachability.h"
#import "PEDUnsupportedConnection.h"
#import "PEDPaymentRequest+Private.h"

@interface PEDConnection()
@end

@implementation PEDConnection

+ (id) developerTestConnectionWithStatusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    NSDictionary * config =
    @{
      @"pedType": kPEDTypeDeveloper,
      @"pedValues": @{}
      };
    
    return [PEDConnection PEDConnectionWithConfiguration:config statusUpdateBlock:statusUpdateBlock configDelegate:configDelegate];
}


+ (id) iZettleTestConnectionWithStatusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    NSDictionary * config =
        @{
          @"pedType": kPEDTypeiZettle,
          @"pedValues": @{
                @"enforcedEmail": @"externaldemo@izettle.com"
            }
        };
    
    return [PEDConnection PEDConnectionWithConfiguration:config statusUpdateBlock:statusUpdateBlock configDelegate:configDelegate];
}

+ (id) adyenTestConnectionWithStatusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    NSDictionary * config =
    @{
      @"pedType": kPEDTypeAdyen,
      @"pedValues": @{
              @"sandbox": @"TRUE",
              @"apiKey": @"AQEmhmfuXNWTK0Qc+iSAk30PrvCTaoRBGYPBxtOKJaYWVPCZr/qZ0k4QwV1bDb7kfNy1WIxIIkxgBw==-GQ5SKCgksasZrUZVPOHO3vPmxgKeeNOoyX1cqU7cCxg=-94AqBk6G6DD46VEa",
              @"merchantAccount": @"PayWithBoltCOM"
              }
      };
    
    return [PEDConnection PEDConnectionWithConfiguration:config statusUpdateBlock:statusUpdateBlock configDelegate:configDelegate];
}

+ (id) monerisTestConnectionWithStatusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    NSDictionary * config =
    @{
      @"pedType": kPEDTypeMoneris,
      @"pedValues": @{
              
              }
      };
    
    return [PEDConnection PEDConnectionWithConfiguration:config statusUpdateBlock:statusUpdateBlock configDelegate:configDelegate];
}

+ (id) usaepayTestConnectionWithStatusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    NSDictionary * config =
    @{
      @"pedType": kPEDTypeUSAEpay,
      @"pedValues": @{}
      };
    
    return [PEDConnection PEDConnectionWithConfiguration:config statusUpdateBlock:statusUpdateBlock configDelegate:configDelegate];
}


+ (id) PEDDemoConnectionWithType:(NSString *)type statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    return [PEDConnection PEDConnectionWithConfiguration:@{@"pedType": type} statusUpdateBlock:statusUpdateBlock configDelegate:configDelegate];
}

+ (id) PEDConnectionWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    if ([[configuration objectForKey:@"pedType"] isEqualToString:kPEDTypeAdyen]) {
        return [[PEDAdyenConnection alloc] initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock configDelegate: configDelegate];
    } else if ([[configuration objectForKey:@"pedType"] isEqualToString:kPEDTypeiZettle]) {
        return [[PEDiZettleConnection alloc] initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock configDelegate: configDelegate];
    } else if ([[configuration objectForKey:@"pedType"] isEqualToString:kPEDTypeMoneris]) {
        return [[PEDMonerisConnection alloc] initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock configDelegate: configDelegate];
    } else if ([[configuration objectForKey:@"pedType"] isEqualToString:kPEDTypeUSAEpay]) {
        return [[PEDUSAEpayConnection alloc] initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock configDelegate: configDelegate];
    } else if ([[configuration objectForKey:@"pedType"] isEqualToString:kPEDTypeSquare]) {
        return [[PEDSquareConnection alloc] initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock configDelegate: configDelegate];
    } else if ([[configuration objectForKey:@"pedType"] isEqualToString:kPEDTypeDeveloper]) {
        return [[PEDDeveloperConnection alloc] initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock];
    } else {
        return [[PEDUnsupportedConnection alloc] initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock];
    }
}

- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock {
    self = [self init];
    self.configuration = configuration;
    self.statusUpdateBlock = statusUpdateBlock;

    // This class must be subclasses - otherwise return invalid pedType
    if ([self class] == [PEDConnection class]) {
        self.status = kPEDStatusErrorSDK;
        self.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:kPEDConnectionErrorInvalidPedType userInfo:@{NSLocalizedDescriptionKey: @"Unsupported PED Type"}];
    }
    
    self.reachability = [PEDReachability reachabilityForInternetConnection];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    [self.reachability startNotifier];
    
    return self;
}

- (NSString *) pedType {
    return @"UNIMPLEMENTED";
}

- (NSString *) pedName {
    return @"Unsupported PED Type";
}
- (NSString *) pedDescription {
    return @"Unsupported PED Type";
}
- (NSDictionary *) currencies {
    return @{};
}
- (BOOL) canRefundFull {
    return false;
}

- (BOOL) canRefundPartial {
    return false;
}

- (BOOL) hideRefreshDeviceList {
    return false;
}

- (NSString *) status {
    if ([_status isEqualToString:kPEDStatusConnectedReady]) {
        if (self.reachability.currentReachabilityStatus == NotReachable) {
            return kPEDStatusConnectedOffline;
        }
    }
    
    return _status;
}


- (PEDPaymentRequest *) startPaymentInCurrency:(NSString*)currency forAmount:(NSDecimalNumber *)amount withReference:(NSString *)reference options:(NSDictionary *)options statusUpdateBlock:(void(^)(PEDPaymentRequest * request))statusUpdateBlock {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (void) cancelPayment:(PEDPaymentRequest *)payment {
    [NSException raise:kPEDCancelPaymentErrorUnsupported
                format:@"Cancelling a payment is unsupported by this PED type"];
}

- (PEDRefundRequest *) refundPaymentReference:(NSString *)reference amount:(NSDecimalNumber *)amount withReason:(NSString *)reason statusUpdateBlock:(void (^)(PEDRefundRequest *))statusUpdateBlock {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

- (void) updateFirmwareOnComplete:(void(^)(BOOL success, NSString *message))completionHandler {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        completionHandler(false, @"Firmware upgrade not supported for this PED type");
    });
}

- (void) reachabilityChanged:(NSObject *) notification {
    NSLog(@"Reachability Changes: %d", (int)self.reachability.currentReachabilityStatus);
    self.statusUpdateBlock(self);
}

- (void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [self.reachability stopNotifier];
}

- (PEDRefundRequest *) refundPaymentTransaction:(NSString*)boltReceipt amount:(NSDecimalNumber *)amount withReason:(NSString *)reason statusUpdateBlock:(void(^)(PEDRefundRequest * request))statusUpdateBlock {
    return nil;
}



@end
