//
//  PEDiZettleConnection.m
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "PEDiZettleConnection.h"
#import "PEDConnection+Private.h"
#import "PEDPaymentRequest.h"
#import "PEDPaymentRequest+Private.h"
#import "PEDRefundRequest.h"
#import "PEDRefundRequest+Private.h"
#import "PEDiZettleAuthorizationProvider.h"
#import "PEDConfigKey.h"
#import "PEDUtil.h"
#import <iZettleSDK/iZettleSDK.h>

static BOOL iZettleSDKStarted;

@implementation PEDiZettleConnection

- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate {
    self = [super initWithConfiguration:configuration statusUpdateBlock:statusUpdateBlock];
    
    [self downloadConfig];

    return self;
}

- (void) downloadConfig {
    if (!self.status || [self.status isEqualToString:kPEDStatusConnectingFailed]) {
        self.status = kPEDStatusConnecting;
        PEDiZettleConnection * _self = self;
        
        NSURL * url = [NSURL URLWithString:[self.configuration valueForKeyPath:@"pedValues.configuration"]];
        
        NSLog(@"PEDiZettleConnection.downloadConfiguration: %@", url.absoluteString);
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if (error) {
                // Dont call callback because we can't handle an error without a PEDConnection
                // On network status change we should try to connect again
                _self.status = kPEDStatusConnectingFailed;
                _self.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Could not connect to server to retrieve credentials"]}];
            } else {
                NSDictionary * config = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                _self.configuration = config;

                dispatch_async( dispatch_get_main_queue(), ^{
                    if (iZettleSDKStarted != true) {
                        NSError *err;
                        iZettleSDKAuthorization *authProvider = [[iZettleSDKAuthorization alloc] initWithClientID:[config objectForKey:@"client_id"]  callbackURL:[NSURL URLWithString:[config objectForKey:@"redirect_url"] ] error:&err enforcedUserAccount:^NSString * _Nullable{
                            return [config objectForKey:@"enforced_email"];
                        }];
//                        PEDiZettleAuthorizationProvider *xauthProvider = [[PEDiZettleAuthorizationProvider alloc] initWithDictionary:config];
                        if (!err) {
                            [iZettleSDK.shared startWithAuthorizationProvider:authProvider];
                            iZettleSDKStarted = true;
                        }
                    }
                    
                    if (iZettleSDKStarted) {
                        _self.status = kPEDStatusConnectedReady;
                    } else {
                        _self.status = kPEDStatusErrorSDK;
                        _self.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:kPEDConnectionErrorInvalidPedImplemention userInfo:@{NSLocalizedDescriptionKey: @"Could not start SDK, probably an invalid API Key."}];
                    }
                    _self.statusUpdateBlock(_self);
                });

            }
        }] resume];
    }
}

- (void) reachabilityChanged:(NSObject *) notification {
    if ([self.status isEqualToString:kPEDStatusConnectingFailed]) {
        [self downloadConfig];
    }
    
    [super reachabilityChanged:notification];
}

- (NSString *) pedType {
    return kPEDTypeiZettle;
}

- (NSString *) pedName {
    return @"Zettle";
}
- (NSString *) pedDescription {
    return [NSString stringWithFormat:@"%@ %@", self.pedName, iZettleSDK.shared.version];
}
- (NSDictionary *) currencies {
    return @{
             @"GBP": @{
                     @"methods": @[kPEDCardTypeMastercard, kPEDCardTypeVisa, kPEDCardTypeAMEX,
                                   kPEDCardTypeMaestro, kPEDCardTypeVPay, kPEDCardTypeVisaElectron,
                                   kPEDCardTypeJCB, kPEDCardTypeDiners],
                     @"minCharge": [NSDecimalNumber decimalNumberWithString:@"1"],
                     @"maxCharge": [NSDecimalNumber decimalNumberWithString:@"5000"]
                     }
             };
}

- (BOOL) canRefundFull {
    return true;
}
- (BOOL) canRefundPartial {
    return true;
}

- (PEDPaymentRequest *) startPaymentInCurrency:(NSString*)currency forAmount:(NSDecimalNumber *)amount withReference:(NSString *)reference options:(NSDictionary *)options statusUpdateBlock:(void (^)(PEDPaymentRequest *))statusUpdateBlock {

    PEDPaymentRequest * request = [[PEDPaymentRequest alloc] initWithConnection:self forAmount:amount inCurrency:currency withReference:reference statusUpdateBlock:statusUpdateBlock];

    if (self.currencies[currency] && [amount compare:self.currencies[currency][@"minCharge"]] == NSOrderedAscending) {
        request.status = kPEDPaymentStatusInvalidAmount;
    } else if (self.currencies[currency] && [amount compare:self.currencies[currency][@"maxCharge"]] == NSOrderedDescending) {
        request.status = kPEDPaymentStatusInvalidAmount;
    } else {
        request.status = kPEDPaymentStatusInProgress;

        [iZettleSDK.shared chargeAmount:amount enableTipping:false reference:reference presentFromViewController:PEDUtil.topMostController completion:^(iZettleSDKPaymentInfo * _Nullable paymentInfo, NSError * _Nullable error) {
            // Set Status
            request.error = error.code == 0 || error.code == 7 ? nil : error;
            request.status = error ? (error.code == 0 || error.code == 7 ? kPEDPaymentStatusCancelled :kPEDPaymentStatusError) : kPEDPaymentStatusSuccess;
            
            if ([request.status isEqualToString:kPEDPaymentStatusSuccess]) {
                // Log to Bolt
                NSLog(@"Transaction successful!");
                request.error = nil;
                request.customerId = [PEDUtil MD5:paymentInfo.panHash];
                request.methodType = paymentInfo.cardBrand;
                request.methodDescription = paymentInfo.obfuscatedPan;
                request.authorisationCode = paymentInfo.authorizationCode;
                request.paymentValues = @{
                                          @"transactionId": request.reference ? request.reference: [NSNull null],

                                          @"paymentMethodType": paymentInfo.cardBrand ? paymentInfo.cardBrand : [NSNull null],
                                          @"paymentMethodEntryMode": paymentInfo.entryMode ? paymentInfo.entryMode : [NSNull null],
                                          @"paymentMethodDetail": paymentInfo.obfuscatedPan ? paymentInfo.obfuscatedPan : [NSNull null],

                                          @"authCode": paymentInfo.authorizationCode ? paymentInfo.authorizationCode : [NSNull null],
                                          @"transactionHandle": paymentInfo.AID ? paymentInfo.AID : [NSNull null],
                                          @"customerId": request.customerId ? request.customerId : [NSNull null]
                                          };
                request.status = kPEDPaymentStatusSuccess;
            }
        }];
    }
    
    return request;
}

- (void) updateFirmwareOnComplete:(void(^)(BOOL success, NSString *message))completionHandler {
    [[iZettleSDK shared] presentSettingsFromViewController:[PEDUtil topMostController]];
}

- (PEDRefundRequest *) refundPaymentReference:(NSString*)reference amount:(NSDecimalNumber *)amount currency:(NSString *)currency withReason:(NSString *)reason statusUpdateBlock:(void(^)(PEDRefundRequest * request))statusUpdateBlock {
    
    PEDRefundRequest * request = [[PEDRefundRequest alloc] initWithConnection:self forTransactionReference:reference forAmount:amount inCurrency:currency withReason:reason statusUpdateBlock:statusUpdateBlock];
    
    bool blockRefunds = false;
    
    if (blockRefunds) {
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"Please use the Zettle app for refunds" preferredStyle:UIAlertControllerStyleAlert];
        [alertVC addAction:okAction];
        [[PEDUtil topMostController] presentViewController:alertVC animated:true completion:^{ }];
        
        request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Not supported."]}];
        request.status = kPEDPaymentStatusError;
        return request;
    }
    
    if (!reference.length) {
        NSLog(@"Refund failed!: Reference Required.");
        request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Reference required."]}];
        request.status = kPEDPaymentStatusError;
    } else if (amount && ([amount isEqual:[NSDecimalNumber notANumber]] || (!amount.doubleValue))) {
        NSLog(@"Refund failed!: Invalid amount.");
        request.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Invalid amount."]}];
        request.status = kPEDPaymentStatusError;
    } else {
        request.status = kPEDPaymentStatusInProgress;

        [iZettleSDK.shared refundAmount:amount ofPaymentWithReference:reference refundReference:nil presentFromViewController:PEDUtil.topMostController completion:^(iZettleSDKPaymentInfo * _Nullable paymentInfo, NSError * _Nullable error) {

            request.error = error.code == 0 || error.code == 7 ? nil : error;
            request.status = error ? (error.code == 0 || error.code == 7 ? kPEDPaymentStatusCancelled :kPEDPaymentStatusError) : kPEDPaymentStatusSuccess;
        }];
    }
    
    return request;
}

- (void) cancelPayment:(PEDPaymentRequest *)payment {
    [iZettleSDK.shared abortOperation];
    payment.status = kPEDPaymentStatusCancelled;
}

@end
