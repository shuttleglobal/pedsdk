//
//  PEDiZettleAuthorizationProvider.h
//  PEDSDK
//
//  Created by Yurii Pikh on 12.08.2022.
//  Copyright © 2022 Pay With Bolt. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <iZettleSDK/iZettleSDK.h>

@interface PEDiZettleAuthorizationProvider : NSObject <iZettleSDKAuthorizationProvider>

- (instancetype)initWithDictionary:(NSDictionary *) dictionary;

@end
