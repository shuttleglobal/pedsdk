//
//  PEDiZettleAuthorizationProvider.m
//  PEDSDK
//
//  Created by Yurii Pikh on 12.08.2022.
//  Copyright © 2022 Pay With Bolt. All rights reserved.
//

#import "PEDiZettleAuthorizationProvider.h"

@interface PEDiZettleAuthorizationProvider() <iZettleSDKAuthorizationProvider>

@property(nonatomic, retain) iZettleSDKOAuthToken *authToken;
@property(nonatomic, retain) NSString *clientId;
@property(nonatomic, retain) NSString *redirectUrl;

@end

@implementation PEDiZettleAuthorizationProvider

- (instancetype)initWithDictionary:(NSDictionary *) dictionary {
    if (self = [super init]) {
        
        NSString *accessToken = dictionary[@"access_token"];
        NSNumber *expiresIn = dictionary[@"expires_at"];
        NSString *refreshToken = dictionary[@"refresh_token"];
        self.clientId = dictionary[@"client_id"];
        self.redirectUrl = dictionary[@"redirect_url"];

        self.authToken = [[iZettleSDKOAuthToken alloc] initWithAccessToken:accessToken expiresIn:expiresIn.longValue refreshToken:refreshToken error:nil];
    }
    
    return self;
}


// MARK: - iZettleSDKAuthorizationProvider

- (void)authorizeAccountWithCompletion:(void (^ _Nonnull)(iZettleSDKOAuthToken * _Nullable, NSError * _Nullable))completion {
    completion(self.authToken, nil);
}

- (void)verifyAccountWithUuid:(NSUUID * _Nonnull)uuid completion:(void (^ _Nonnull)(iZettleSDKOAuthToken * _Nullable, NSError * _Nullable))completion {
    completion(self.authToken, nil);
}

@end
