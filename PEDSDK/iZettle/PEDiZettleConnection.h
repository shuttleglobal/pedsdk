//
//  PEDiZettleConnection.h
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//
// See:
// https://github.com/iZettle/sdk-ios

#import "PEDConfigDelegate.h"
#import "PEDConnection.h"

@interface PEDiZettleConnection : PEDConnection

- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate;

@end
