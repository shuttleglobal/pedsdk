//
//  PEDPaymentRequest+Private.h
//  pedsdk
//
//  Created by Phil Peters on 23/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#ifndef PEDPRefundRequest_Private_h
#define PEDPRefundRequest_Private_h
#import "PEDRefundRequest.h"

@interface PEDRefundRequest()

@property (nonatomic, copy) void (^statusUpdateBlock)(PEDRefundRequest *request);

@property (nonatomic, retain) NSString * currency;
@property (nonatomic, retain) NSDecimalNumber * amount;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * methodType;
@property (nonatomic, retain) NSString * methodDescription;
@property (nonatomic, retain) NSString * authorisationCode;
@property (nonatomic, retain) NSString * gatewayReference;
@property (nonatomic, readonly) NSString * nonce;
@property (nonatomic, retain) NSDictionary * paymentValues;
@property (nonatomic, retain) NSError * error;

@end

#endif /* PEDPRefundRequest_Private_h */
