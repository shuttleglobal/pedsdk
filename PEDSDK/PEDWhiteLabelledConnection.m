//
//  PEDWhiteLabelledConnection.m
//  PEDSDK
//
//  Created by Phil Peters on 11/03/2019.
//  Copyright © 2019 Pay With Bolt. All rights reserved.
//

#import "PEDWhiteLabelledConnection.h"
#import "PEDPaymentRequest+Private.h"
#import "PEDPaymentViewController.h"
#import "PEDUtil.h"

@interface PEDWhiteLabelledConnection()  <UIPopoverPresentationControllerDelegate>
@end

@implementation PEDWhiteLabelledConnection

-(void) refreshDeviceListOnCompletion:(void (^)(PEDWhiteLabelledConnection *)) completionBlock {
    NSLog(@"refreshDeviceList Unimplemented");
}

- (NSArray *) deviceList {
    return @[@{
                 @"id": @"default",
                 @"name": @"Default"
                 }];
}

- (NSDictionary *) selectedDevice {
    NSString * deviceId = [[NSUserDefaults standardUserDefaults] stringForKey:@"PEDSDKDeviceId"];
    
    for (NSDictionary * device in self.deviceList) {
        if ([device[@"id"] isEqualToString:deviceId]) {
            return device;
        }
    }
    
    return nil;
}

- (void) setSelectedDevice:(NSDictionary *)device {
    PEDWhiteLabelledConnection * _self = self;
    [[NSUserDefaults standardUserDefaults] setObject:device[@"id"] forKey:@"PEDSDKDeviceId"];
    if ([NSThread currentThread].isMainThread) {
        [self.paymentViewController.view setNeedsLayout];
    } else {
        dispatch_async( dispatch_get_main_queue(), ^{
            [_self.paymentViewController.view setNeedsLayout];
        });
    }
}

- (void) setSelectedDevice:(NSDictionary *)device duringActivePaymentRequest:(PEDPaymentRequest *) paymentRequest{
    if ([paymentRequest.status isEqual:@"INPROGRESS"]) {
        typedef void (^statusUpdateBlock)(PEDPaymentRequest *);
        statusUpdateBlock oldCallback = paymentRequest.statusUpdateBlock;
        
        PEDWhiteLabelledConnection *_self = self;
        
        paymentRequest.statusUpdateBlock = ^(PEDPaymentRequest * request){
            NSLog(@"Cancelled, starting new payment");
            request.statusUpdateBlock = oldCallback;
            [_self setSelectedDevice:device];
            
            [request updateNonce];
            [_self startPaymentRequest:request];
        };
        
        [self cancelPayment:paymentRequest];
    } else {
        [self setSelectedDevice:device];
        
        [paymentRequest updateNonce];
        [self startPaymentRequest:paymentRequest];
    }
}

- (PEDPaymentRequest *) startPaymentInCurrency:(NSString*)currency forAmount:(NSDecimalNumber *)amount withReference:(NSString *)reference options:(NSDictionary *)options statusUpdateBlock:(void (^)(PEDPaymentRequest *))statusUpdateBlock {
    
    PEDPaymentRequest * paymentRequest = [[PEDPaymentRequest alloc] initWithConnection:self forAmount:amount inCurrency:currency withReference:reference statusUpdateBlock:statusUpdateBlock];
    
    if ([amount compare:NSDecimalNumber.zero] == NSOrderedAscending) {
        paymentRequest.status = kPEDPaymentStatusInvalidAmount;
    } else {
        self.paymentViewController = [[PEDPaymentViewController alloc] initWithPaymentRequest:paymentRequest forConnection:self];
        self.navigationViewController = [[UINavigationController alloc] initWithRootViewController:self.paymentViewController];
        
        self.navigationViewController.modalPresentationStyle = UIModalPresentationPopover;
        self.navigationViewController.popoverPresentationController.delegate = self;
        self.navigationViewController.popoverPresentationController.permittedArrowDirections = 0;
        self.navigationViewController.popoverPresentationController.sourceView = PEDUtil.topMostController.view;
        self.navigationViewController.popoverPresentationController.sourceRect = CGRectMake(PEDUtil.topMostController.view.bounds.size.width / 2, PEDUtil.topMostController.view.bounds.size.height / 2, 0, 0);
        
        [PEDUtil.topMostController presentViewController:self.navigationViewController animated:TRUE completion:nil];
        
        if (!self.selectedDevice) {
            paymentRequest.status = kPEDPaymentStatusError;
            paymentRequest.error = [NSError errorWithDomain:@"com.paywithbolt.pedsdk" code:0 userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Please select a card reader."]}];
            return paymentRequest;
        } else {
            paymentRequest.status = kPEDPaymentStatusInProgress;
        }
        
        [self startPaymentRequest:paymentRequest];
    }
    
    return paymentRequest;
}

- (void) startPaymentRequest:(PEDPaymentRequest*)paymentRequest {
    NSLog(@"startPaymentRequest: Unimplemented");
};

@end
