//
//  PEDPaymentRequest.h
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kPEDPaymentStatusInProgress @"INPROGRESS"
#define kPEDPaymentStatusCancelled @"CANCELLED"
#define kPEDPaymentStatusError @"ERROR"
#define kPEDPaymentStatusSuccess @"SUCCESS"
#define kPEDPaymentStatusDeclined @"DECLINED"
#define kPEDPaymentStatusInvalidAmount @"INVALIDAMOUNT"

typedef NS_ENUM(NSUInteger, PEDPaymentError) {
    kPEDPaymentErrorGeneral
};

@class PEDConnection;

@interface PEDPaymentRequest : NSObject

@property (nonatomic, readonly) PEDConnection * connection;
@property (nonatomic, readonly) NSString * currency;
@property (nonatomic, readonly) NSDecimalNumber * amount;
@property (nonatomic, readonly) NSString * reference;
@property (nonatomic, readonly) NSString * customerId;
@property (nonatomic, readonly) NSString * status;

@property (nonatomic, readonly) NSString * methodType;
@property (nonatomic, readonly) NSString * methodDescription;
@property (nonatomic, readonly) NSString * authorisationCode;
@property (nonatomic, readonly) NSString * gatewayReference;
@property (nonatomic, readonly) NSDictionary * paymentValues;
@property (nonatomic, readonly) NSError * error;

- (void) updateNonce;

- (id) initWithConnection:(PEDConnection *) connection forAmount:(NSDecimalNumber *) amount inCurrency:(NSString *) currency withReference:(NSString *)reference statusUpdateBlock:(void(^)(PEDPaymentRequest * request))statusUpdateBlock;

@end
