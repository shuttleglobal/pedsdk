//
//  PEDSquareConnection.h
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//
// See:
// - Add items to the plist file
// - A n]dd the run script to the project file

#import "PEDConfigDelegate.h"
#import "PEDConnection.h"

@interface PEDSquareConnection : PEDConnection

- (id) initWithConfiguration:(NSDictionary *)configuration statusUpdateBlock:(void (^)(PEDConnection *))statusUpdateBlock configDelegate:(id<PEDConfigDelegate>)configDelegate;

@end
