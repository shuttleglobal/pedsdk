//
//  PEDUtil.m
//  PEDSDK
//
//  Created by Phil Peters on 30/01/2019.
//  Copyright © 2019 Pay With Bolt. All rights reserved.
//

#import "PEDUtil.h"
#import <CommonCrypto/CommonDigest.h>

@implementation PEDUtil

+ (UIViewController*) topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

+ (NSString *)MD5:(NSString *)string {
    const char *cstr = [string UTF8String];
    unsigned char result[16];
    CC_MD5(cstr, (CC_LONG)strlen(cstr), result);
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

+ (void) alertWithTitle:(NSString *)title message:(NSString *)message  {
    UIAlertController * vc = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];

    [vc addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [vc dismissViewControllerAnimated:true completion:^{
        }];
    }]];

    dispatch_async( dispatch_get_main_queue(), ^{
        [[PEDUtil topMostController] presentViewController:vc animated:TRUE completion:^{
        }];
    });
}

@end
