//
//  PEDConfigKey.h
//  BrightpearlPOS
//
//  Created by Paul Keith on 02/07/2018.
//  Copyright © 2018 Brightpearl. All rights reserved.
//

#ifndef PEDConfigKey_h
#define PEDConfigKey_h

// The key used to store the iZettle ClientID key.
extern NSString *const kiZettleClientId;

// The key used to store the iZettle CallbackURL key.
extern NSString *const kiZettleCallbackURL;

#endif /* PEDConfigKey_h */
