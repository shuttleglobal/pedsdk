//
//  pedsdk.h
//  pedsdk
//
//  Created by Phil Peters on 22/04/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for pedsdk.
FOUNDATION_EXPORT double pedsdkVersionNumber;

//! Project version string for pedsdk.
FOUNDATION_EXPORT const unsigned char pedsdkVersionString[];

#import "PEDConnection.h"
#import "PEDPaymentRequest.h"
#import "PEDRefundRequest.h"
#import "PEDConfigDelegate.h"

