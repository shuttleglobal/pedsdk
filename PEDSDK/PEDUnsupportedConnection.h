//
//  PEDUnsupportedConnection.h
//  PEDSDK
//
//  Created by Phil Peters on 19/09/2016.
//  Copyright © 2016 Pay With Bolt. All rights reserved.
//

#import "PEDConnection.h"

@interface PEDUnsupportedConnection : PEDConnection

@end
